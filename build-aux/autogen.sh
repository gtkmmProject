#!/bin/sh -xe

libtoolize                      $LT_EXTRA
aclocal-1.9                     $ACLOCAL_EXTRA
autoheader                      $AH_EXTRA
automake-1.9 --add-missing      $AM_EXTRA
autoconf                        $AC_EXTRA

