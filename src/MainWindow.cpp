#include <glibmm/i18n.h>
#include <gtk/gtk.h>
#include <libglademm/xml.h>

#include <gtkmm/menu.h>
#include <gtkmm/menuitem.h>
#include <gtkmm/accelkey.h>
#include <gtkmm/stock.h>
#include <gtkmm/menubar.h>
#include <gtkmm/menushell.h>
#include <gtkmm/toolbar.h>
#include <gtkmm/uimanager.h>

#include <iostream>

#include "MainWindow.h"
#include "TestWidget.h"
#include "GraphWindow.h"

using std::cout;

MainWindow :: MainWindow (void) :
    gW (0)
{
    set_title (_("Main window"));
    set_border_width (5);
    set_default_size (800, 600);

    add (vBox);

    pActionGroup  = Gtk::ActionGroup::create();
    pActionGroup->add (Gtk::Action::create ("FileMenu", _("_File")));
    pActionGroup->add (Gtk::Action::create ("FileNew", Gtk::Stock::NEW), sigc::mem_fun (*this, &MainWindow::fileNew));
    Glib::RefPtr <Gtk::Action> fileOpenAct = Gtk::Action::create ("FileOpen", Gtk::Stock::OPEN);//, _("_Open"), _("Open file from current path"));
    Gtk::AccelKey fileOpenKey ('O', Gdk::CONTROL_MASK);
//        ("<Control>o");('O', Gdk::CONTROL_MASK);//, "<MainWindow>/_File/Open" );
    pActionGroup->add ( fileOpenAct, fileOpenKey,
            sigc::mem_fun (*this, &MainWindow::fileOpen));
    fileOpenAct->connect_accelerator ();
    pActionGroup->add (Gtk::Action::create ("FileQuit", Gtk::Stock::QUIT),
            sigc::mem_fun (*this, &MainWindow::fileQuit));
    //Gtk::MenuItem fileItem (Glib::ustring(_("File")), true);
    //menuBar.append (fileItem);
    pActionGroup->add (Gtk::Action::create ("EditMenu", "_Edit"));
    pActionGroup->add (Gtk::Action::create ("EditCut", Gtk::Stock::CUT),
            sigc::mem_fun (*this, &MainWindow::editCut));
    pActionGroup->add (Gtk::Action::create ("EditCopy", Gtk::Stock::COPY),
            sigc::mem_fun (*this, &MainWindow::editCopy));
    pActionGroup->add (Gtk::Action::create ("EditPaste", Gtk::Stock::PASTE),
            sigc::mem_fun (*this, &MainWindow::editPaste));
    //
    // Data menu
    //
    Glib::RefPtr <Gtk::Action> DataAction = Gtk::Action::create ("DataMenu", "_Data");
    pActionGroup->add (DataAction, Gtk::AccelKey ("<Alt>d"));
    Glib::RefPtr <Gtk::Action> DataViewAction = Gtk::Action::create ("DataView", "_View");
    pActionGroup->add (DataViewAction, Gtk::AccelKey("<Alt>v"),
            sigc::mem_fun (*this, &MainWindow::dataView));
    Glib::RefPtr <Gtk::Action> DataGraphAction = Gtk::Action::create ("DataGraph", "_Graph");
    pActionGroup->add (DataGraphAction, Gtk::AccelKey ("<Alt>g"),
            sigc::mem_fun (*this, &MainWindow::graphView));
    //
    //Help menu:
    //
    //
    pActionGroup->add (Gtk::Action::create("HelpMenu", "_Help") );
    pActionGroup->add (Gtk::Action::create("HelpAbout", Gtk::Stock::HELP),
          sigc::mem_fun(*this, &MainWindow::onHelp));

    pUIManager = Gtk::UIManager::create ();
    pUIManager->insert_action_group (pActionGroup);

    add_accel_group (pUIManager->get_accel_group());

    Gtk::UIManager::ui_merge_id m_uid0 = pUIManager->add_ui_from_file ("menu.xml");
    pUIManager->ensure_update ();

    pMenuBar = (Gtk::MenuBar *)pUIManager->get_widget("/MenuBar");
    DataAction->set_accel_group (this->get_accel_group());
    DataViewAction->set_accel_group (this->get_accel_group());
    DataGraphAction->set_accel_group (this->get_accel_group());
    Gtk::MenuItem *dataItem = DataAction->create_menu_item ();
    Gtk::Menu *dVMenu = new Gtk::Menu ();
    dataItem->set_submenu (*dVMenu);
    Gtk::MenuItem *dataViewItem = DataViewAction->create_menu_item ();
    pMenuBar->insert (*dataItem, 2);
    dVMenu->append (*dataViewItem);
    Gtk::MenuItem *dataGraphItem = DataGraphAction->create_menu_item ();
    dVMenu->append (*dataGraphItem);

    pToolBar = (Gtk::Toolbar *)pUIManager->get_widget ("/ToolBar");

    vBox.pack_start (*pMenuBar, Gtk::PACK_SHRINK);
    vBox.pack_start (*pToolBar, Gtk::PACK_SHRINK);
    frame.set_size_request (-1, 500);
    vBox.pack_start (frame);
    vBox.pack_start (statusBar);

    show_all_children ();
}

MainWindow :: ~MainWindow (void)
{
    if (gW)
    {
        delete gW;
        gW = 0;
    }
}

void MainWindow :: fileNew (void)
{
    cout << __FUNCTION__ << ' ' << "File new" << '\n';
}

void MainWindow :: fileOpen (void)
{
    cout << __FUNCTION__ << ' ' << "File open" << '\n';
}

void MainWindow :: fileQuit (void)
{
    cout << __FUNCTION__ << ' ' << "Quit" << '\n';
    this->hide ();
}

void MainWindow :: editCut (void)
{
    cout << __FUNCTION__ << ' ' << "Edit cut" << '\n';
}

void MainWindow :: editCopy (void)
{
    cout << __FUNCTION__ << ' ' << "Edit copy" << '\n';
}

void MainWindow :: editPaste (void)
{
    cout << __FUNCTION__ << ' ' << "Edit paste" << '\n';
}

void MainWindow :: onHelp (void)
{
    cout << __FUNCTION__ << ' ' << "Help" << '\n';
}

void MainWindow :: dataView (void)
{
    cout << __FUNCTION__ << ' ' << "Data view" << '\n';
    TestWidget *tWidget = new TestWidget ();
    tWidget->set_modal (true);
    tWidget->show ();
    tWidget->raise ();
    Gtk::Main::run (*tWidget);

//    delete tWidget;
}

void MainWindow :: graphView (void)
{
    cout << __FUNCTION__ << ' ' << "Graph" << std::endl;
    Glib::RefPtr<Gnome::Glade::Xml> refXml (0);
#ifdef GLIBMM_EXCEPTIONS_ENABLED
    try
    {
        refXml = Gnome::Glade::Xml::create ("ui/GraphWidget.glade", "GraphWindow");
    }
    catch (const Gnome::Glade::XmlError& ex)
    {
        std::cerr << ex.what() << std::endl;
        return;
    }
#else
    std::auto_ptr<Gnome::Glade::XmlError> error;
    refXml = Gnome::Glade::Xml::create ("ui/GraphWidget.glade", "GraphWindow", "", error);
    if (error.get())
    {
        std::cerr << error->what() << std::endl;
        return;
    }
#endif

    refXml->get_widget_derived ("GraphWindow", gW);

    if (gW)
    {
        Gtk::Main::run (*gW);
        //this->show_all ();
        delete gW;
    }

    gW = 0;
}
