#include <gtkmm/label.h>
#include <gtkmm/separator.h>

#include <iostream>
#include "EventBox.h"
#include <cstring>

NewEventBox::NewEventBox() :
    Gtk::EventBox ()
{
   add (vBox);
   vBox.pack_start (hBoxUp);
   vBox.pack_start (hBoxDown);
   Gtk::Label* lB0 = new Gtk::Label ("rrr");
   hBoxUp.pack_start (*lB0);
   for (int i=0; i<5; i++)
   {
       Gtk::Widget* w = new Gtk::HSeparator;
       hBoxUp.pack_start (*w);
   }
   Gtk::Label* lB1 = new Gtk::Label ("ttt");
   hBoxUp.pack_start (*lB1);
//   hbox.add(btn1);
//   hbox.add(btn2);
   show_all();
}

bool NewEventBox::on_expose_event (GdkEventExpose* event)
{
   bool rv = Gtk::EventBox::on_expose_event (event);
   int scale = 1;
   int width = get_allocation().get_width();
   int height = get_allocation().get_height();
   double scale_x = (double)width / scale;
   double scale_y = (double)height / scale;

   Cairo::RefPtr<Cairo::Context> cr = this->get_window()->create_cairo_context();

//cr->move_to(get_allocation().get_width()/2,get_allocation().get_height()/2);
   cr->save ();
   cr->translate (width/2.0, height/2.0);
   double radius = 400;//width < height ? width-40 : height-40;
   radius /= 2.0;
   cr->scale (radius, radius);
   cr->arc (0.0, 0.0, 1.0, 0.0, 2 * 3.14);
   cr->arc_negative (0.0, 0.0, 1.0, 0.0, 2 * 3.14);
   cr->fill ();
   cr->restore ();
   cr->stroke ();

   return rv;
}

void NewEventBox::on_realize (void)
{
    //
    // Base class call
    //
    Gtk::EventBox::on_realize ();

    ensure_style ();

    std::cout << __FUNCTION__ << std::endl;

    //
    // Create window
    //

    GdkWindowAttr attributes;
    memset (&attributes, 0, sizeof(attributes));

    Gtk::Allocation allocation = get_allocation ();

    //
    // Set initial position and size of the Gdk::Window:
    //
    attributes.x = allocation.get_x();
    attributes.y = allocation.get_y();
    attributes.width = allocation.get_width();
    attributes.height = allocation.get_height();

    attributes.event_mask = get_events () | Gdk::EXPOSURE_MASK; 
    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.wclass = GDK_INPUT_OUTPUT;

    //
    // set colors
    //
    Gdk::Color bgCol;
    bgCol.set_rgb_p (0.7, 0.4, 0.4);
    Gdk::Color fgCol;
    fgCol.set_rgb_p (0.0, 0.0, 0.0);
    modify_bg (Gtk::STATE_NORMAL , bgCol);
    modify_fg (Gtk::STATE_NORMAL , fgCol);
}
