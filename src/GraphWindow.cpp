#include "EventBox.h"
#include "GraphWindow.h"

GraphWindow :: GraphWindow (BaseObjectType* cobject, const Glib::RefPtr<Gnome::Glade::Xml>& refGlade)
    : Gtk::Window (cobject),
    pRefGlade (refGlade),
    nEventBox (new NewEventBox)
{
    //
    // Main window
    //
    pRefGlade->get_widget ("GraphWindow");
    set_size_request (400, 400);

    //
    // Child widgets
    //
    pRefGlade->get_widget ("vbox1", vBox);
    pRefGlade->get_widget ("hbox1", hBox);

    hBox->pack_start (*nEventBox);

    show_all_children ();
}

GraphWindow :: ~GraphWindow (void)
{
}

void GraphWindow :: gQuit (void)
{
    this->hide ();
}
