#ifndef _GTK_TestWidget_H
#define _GTK_TestWidget_H

#include <gtkmm.h>
#include <gtkmm/window.h>
#include <gtkmm/treeview.h>
#include <gtkmm/button.h>
#include <gtkmm/treemodel.h>
#include <gtkmm/treestore.h>

#include <iostream>

using std::cout;

class TestWidget : public Gtk::Window
{
    public:
        TestWidget (void);
        virtual ~TestWidget (void);

    private:
        //
        // Signal handlers
        //
        virtual void onCancel (void);
        virtual void onApply (void);
        virtual void onOk (void);
        virtual void on_treeview_row_activated (const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column);

    private:
        //
        // Variables
        //
        class ModelColumns : public Gtk::TreeModel::ColumnRecord
        {
            public:
                ModelColumns() { add(m_col_id); add(m_col_name); }

            Gtk::TreeModelColumn<int> m_col_id;
            Gtk::TreeModelColumn<Glib::ustring> m_col_name;
        };

        ModelColumns m_Columns;

        Gtk::TreeView treeView;
        Glib::RefPtr<Gtk::TreeStore> pTreeModel;

        Gtk::VBox vBox;
        Gtk::HPaned hPaned;
        Gtk::Frame frame;
        Gtk::ScrolledWindow scWindow;
        Gtk::HButtonBox buttonBox;
        Gtk::Button pbOk;
        Gtk::Button pbCancel;
        Gtk::Button pbApply;
};

#endif
