#include "TestWidget.h"

TestWidget :: TestWidget (void) :
    pbOk ("OK"),
    pbCancel ("Cancel"),
    pbApply ("Apply")
{
    pbOk.set_flags (Gtk::CAN_DEFAULT);
    set_title("Gtk::TreeView (TreeStore) example");
    set_border_width (5);
    set_default_size (600, 400);
    set_modal (true);
    set_default (pbOk);

    add(vBox);

    scWindow.add (treeView);
    scWindow.set_policy (Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

    hPaned.pack1 (scWindow, Gtk::EXPAND);
    hPaned.pack2 (frame, Gtk::EXPAND);
    frame.set_size_request (100, 100);
    vBox.pack_start (hPaned);
    vBox.pack_start (buttonBox, Gtk::PACK_SHRINK);

    buttonBox.pack_start (pbOk, Gtk::PACK_SHRINK);
    buttonBox.pack_start (pbCancel, Gtk::PACK_SHRINK);
    buttonBox.pack_start (pbApply, Gtk::PACK_SHRINK);
    pbOk.signal_clicked().connect( sigc::mem_fun(*this, &TestWidget::onOk) );
    pbCancel.signal_clicked().connect( sigc::mem_fun(*this, &TestWidget::onCancel) );
    pbApply.signal_clicked().connect( sigc::mem_fun(*this, &TestWidget::onApply) );

    buttonBox.set_border_width (5);
    buttonBox.set_layout(Gtk::BUTTONBOX_END);

//    treeView.show ();
//    m_Columns = ModelColumns();
    pTreeModel = Gtk::TreeStore::create (m_Columns);
    treeView.set_reorderable();
    //
    // Fill the TreeView's model
    //
    Gtk::TreeModel::Row row = *(pTreeModel->append());
    row[m_Columns.m_col_id] = 1;
    row[m_Columns.m_col_name] = "Billy Bob";

    Gtk::TreeModel::Row childrow = *(pTreeModel->append(row.children()));
    childrow[m_Columns.m_col_id] = 11;
    childrow[m_Columns.m_col_name] = "Billy Bob Junior";

    childrow = *(pTreeModel->append(row.children()));
    childrow[m_Columns.m_col_id] = 12;
    childrow[m_Columns.m_col_name] = "Sue Bob";

    row = *(pTreeModel->append());
    row[m_Columns.m_col_id] = 2;
    row[m_Columns.m_col_name] = "Joey Jojo";

    row = *(pTreeModel->append());
    row[m_Columns.m_col_id] = 3;
    row[m_Columns.m_col_name] = "Rob McRoberts";

    childrow = *(pTreeModel->append(row.children()));
    childrow[m_Columns.m_col_id] = 31;
    childrow[m_Columns.m_col_name] = "Xavier McRoberts";

    //
    //Add the TreeView's view columns:
    //
    treeView.append_column("ID", m_Columns.m_col_id);
    treeView.append_column("Name", m_Columns.m_col_name);

    treeView.set_model (pTreeModel);
    //
    // Connect signal:
    //
    treeView.signal_row_activated().connect (sigc::mem_fun (*this, &TestWidget::on_treeview_row_activated) );

    show_all_children ();
}

TestWidget :: ~TestWidget (void)
{
}

void TestWidget :: onCancel (void)
{
    cout << "Cancel pressed" << '\n';
    hide ();
}

void TestWidget :: onApply (void)
{
    cout << "Apply pressed" << '\n';
}

void TestWidget :: onOk (void)
{
    cout << "OK pressed" << '\n';
    hide ();
}

void TestWidget :: on_treeview_row_activated (const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* /*column*/)
{
    Gtk::TreeModel::iterator iter = pTreeModel->get_iter (path);
    if (iter)
    {
        Gtk::TreeModel::Row row = *iter;
        std::cout << "Row activated: ID=" << row[m_Columns.m_col_id] << ", Name="
        << row[m_Columns.m_col_name] << std::endl;
    }
}
