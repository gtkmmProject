#ifndef _GTK_MainWindow_H
#define _GTK_MainWindow_H

#include <gtkmm/window.h>
#include <gtkmm/box.h>
#include <gtkmm/frame.h>
#include <gtkmm/statusbar.h>
#include <gtkmm/alignment.h>
#include <gtkmm/uimanager.h>

namespace Gtk
{
    class MenuBar;
    class Toolbar;
};

//using Gtk::MenuBar;
//using Gtk::Toolbar;
class GraphWindow;

class MainWindow : public Gtk::Window
{
    public:
        MainWindow (void);
        virtual ~MainWindow (void);

    private:
        //
        // functions
        //
        virtual void fileNew (void);
        virtual void fileOpen (void);
        virtual void fileQuit (void);
        virtual void editCut (void);
        virtual void editCopy (void);
        virtual void editPaste (void);
        virtual void dataView (void);
        virtual void graphView (void);
        virtual void onHelp (void);

    private:
        //
        // variables
        //
        Gtk::VBox vBox;
        Gtk::Alignment align;
        Gtk::MenuBar* pMenuBar;
        Gtk::Toolbar* pToolBar;
        Gtk::Frame frame;
        Gtk::Statusbar statusBar;

        Glib::RefPtr<Gtk::UIManager> pUIManager;
        Glib::RefPtr<Gtk::ActionGroup> pActionGroup;
        GraphWindow *gW;
};

#endif
