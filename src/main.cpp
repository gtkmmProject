#include <gtkmm/main.h>
#include "MainWindow.h"
//#include "testWidget.h"

int main (int argc, char *argv[])
{
    Gtk::Main kit (argc, argv);

    MainWindow mainWindow;
    Gtk::Main::run (mainWindow);
    //TestWidget tWidget;
    //Gtk::Main::run (tWidget);

    return 0;
}
