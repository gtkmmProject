#ifndef _GraphWindow_H
#define _GraphWindow_H

#include <gtkmm/window.h>
#include <gtkmm/box.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/treeview.h>
#include <libglademm.h>

class NewEventBox;

class GraphWindow : public Gtk::Window
{
    public:
        GraphWindow (BaseObjectType* cobject, const Glib::RefPtr<Gnome::Glade::Xml>& refGlade);
        virtual ~GraphWindow (void);

        virtual void gQuit (void);

    private:
        Glib::RefPtr<Gnome::Glade::Xml> pRefGlade;
        NewEventBox *nEventBox;

        Gtk::VBox *vBox;
        Gtk::HBox *hBox;
};

#endif
