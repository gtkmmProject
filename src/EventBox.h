#ifndef _NEW_EVENT_BOX_H
#define _NEW_EVENT_BOX_H

#include <gtkmm/box.h>
#include <gtkmm/eventbox.h>

class NewEventBox : public Gtk::EventBox
{
    private:
//        Gtk::Button btn1;
//        Gtk::Button btn2;
        Gtk::VBox vBox;
        Gtk::HBox hBoxUp;
        Gtk::HBox hBoxDown;

    public :
       NewEventBox();
       virtual ~NewEventBox() {}

    protected:
       virtual bool on_expose_event(GdkEventExpose* e);
       virtual void on_realize (void);
};

#endif
